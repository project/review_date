<?php

namespace Drupal\review_date\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides a 'Last updated / reviewed' Block.
 *
 * @Block(
 *   id = "last_updated",
 *   admin_label = @Translation("Last updated / reviewed"),
 * )
 */
class ReviewDateBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $entity = NULL;
    foreach (\Drupal::routeMatch()->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $entity = $param;
        break;
      }
    }
    if (!$entity) {
      return $build;
    }
    if (isset($entity->changed)) {
      $changed = DrupalDateTime::createFromTimestamp($entity->changed->value);
      $build['#changed'] = $changed->format('j F Y');
    }
    if (isset($entity->review_date)) {
      $reviewed = new DrupalDateTime($entity->review_date->value);
      $build['#reviewed'] = $reviewed->format('j F Y');
    }
    if ($build) {
      $build['#theme'] = 'review_date';
    }

    return $build;
  }

}
