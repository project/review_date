<?php

namespace Drupal\review_date\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;

/**
 * Plugin implementation of the 'review_date_datetime' widget.
 *
 * @FieldWidget(
 *   id = "review_date_datetime",
 *   label = @Translation("Datetime Timestamp"),
 *   field_types = {
 *     "review_date",
 *   }
 * )
 */
class ReviewDateDatetimeWidget extends DateTimeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $this->removeFieldsetWrapper($element);
    return $element;
  }

  /**
   * Removes the fieldset wrapper and restores the label and description.
   *
   * @param array $element
   *   The element to operate on.
   */
  protected function removeFieldsetWrapper(array &$element): void {
    $key = array_search('fieldset', $element['#theme_wrappers']);
    if ($key === FALSE) {
      return;
    }
    unset($element['#theme_wrappers'][$key]);
    $element['value']['#title'] = $this->fieldDefinition->getLabel();
    $element['value']['#description'] = $this->fieldDefinition->getDescription();
  }

}
