<?php

namespace Drupal\review_date\Plugin\Field\FieldFormatter;

use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;

/**
 * Plugin implementation of the formatter for 'review_date' fields.
 *
 * @FieldFormatter(
 *   id = "review_date_default",
 *   label = @Translation("Review date"),
 *   field_types = {
 *     "review_date"
 *   }
 * )
 */
class ReviewDateFormatter extends DateTimeDefaultFormatter {

}
