<?php

namespace Drupal\review_date\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the 'review_date' entity field type.
 *
 * Defaults to the date the item was first published, or allows the author to
 * specify another time.
 *
 * @FieldType(
 *   id = "review_date",
 *   label = @Translation("Review date"),
 *   description = @Translation("An entity field containing a timestamp of when the entity has been last reviewed."),
 *   no_ui = TRUE,
 *   default_widget = "review_date_datetime",
 *   default_formatter = "review_date_default",
 * )
 *
 * @see \Drupal\Core\Entity\EntityChangedInterface
 */
class ReviewDateItem extends DateTimeItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['value']->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    // If the review date is empty and the item is published we can set the
    // review date to the request time.
    if (!empty($this->value)) {
      return;
    }
    // Make sure we don't save the value 0 as it is a valid timestamp.
    if (!$this->isPublished()) {
      $this->value = NULL;
      return;
    }
    $timestamp = \Drupal::time()->getRequestTime();
    $format = self::DATETIME_STORAGE_FORMAT;
    $date = \Drupal::service('date.formatter')->format($timestamp, 'custom', $format, 'UTC');
    $this->setValue($date);
  }

  /**
   * Determines if the current entity is published.
   *
   * @return bool
   *   TRUE if published, FALSE if not published.
   */
  protected function isPublished(): bool {
    $entity = $this->getEntity();
    if (!$entity instanceof EntityPublishedInterface) {
      return FALSE;
    }
    // TODO: If entity is revisionable check if current revision is published.
    return $entity->isPublished();
  }

}
